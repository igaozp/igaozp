![Banner](banner.png#gh-dark-mode-only)
![Banner](banner-light.png#gh-light-mode-only)

### Hi there 👋

- 👉My personal site [https://andornot.xyz](https://andornot.xyz)
- 💻Computer knowledge wiki [https://okcomputer.wiki](https://okcomputer.wiki)

<div>
  <p align="center">
<!--     <img width="49%" src="https://github-readme-stats.vercel.app/api?username=igaozp&show_icons=true&theme=gruvbox#gh-dark-mode-only" /> -->
    <img width="49%" src="https://github-readme-stats.vercel.app/api?username=igaozp&show_icons=true&theme=default#gh-light-mode-only" />
<!--     <img width="49%" src="https://github-readme-streak-stats.herokuapp.com?user=igaozp&theme=gruvbox&hide_border=true&date_format=%5BY.%5Dn.j#gh-dark-mode-only" /> -->
    <img width="49%" src="https://github-readme-streak-stats.herokuapp.com?user=igaozp&theme=default&date_format=%5BY.%5Dn.j#gh-dark-mode-only" />
  </p>  
</div>
